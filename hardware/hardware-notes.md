# ESP8266
Taken from the ESP8266EX datasheet:
* Operating voltage 2.5 .. 3.6V
* No ripple requirement given
* 170mA max input current (plus I/O)

# Buck converter

Calculator tool see http://www.nomad.ee/micros/mc34063a/

* Vin = 12 V
* Vout = 3.3 V
* Iout = 300 mA
* Vripple = 30 mV
* Fmin = 100 kHz

