EESchema Schematic File Version 4
LIBS:colorNode-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:IRF7403 Q1
U 1 1 5AB62754
P 8250 1100
F 0 "Q1" H 8600 1200 50  0000 L CNN
F 1 "IRF7401" H 8600 1100 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 1000 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 1100 50  0001 L CNN
F 4 "Digikey" H 8600 1200 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 1200 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 1200 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 1200 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 1200 50  0001 L CNN "Manufacturer Part Number"
	1    8250 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5AB62AE6
P 8000 1300
F 0 "R1" V 8080 1300 50  0000 C CNN
F 1 "10k" V 8000 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 1300 50  0001 C CNN
F 3 "" H 8000 1300 50  0001 C CNN
F 4 "Digikey" V 8080 1300 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 1300 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 1300 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 1300 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 1300 50  0001 C CNN "Manufacturer Part Number"
	1    8000 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5AB62B83
P 8000 1450
F 0 "#PWR02" H 8000 1200 50  0001 C CNN
F 1 "GND" H 8000 1300 50  0000 C CNN
F 2 "" H 8000 1450 50  0001 C CNN
F 3 "" H 8000 1450 50  0001 C CNN
	1    8000 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female X2
U 1 1 5AB62C7C
P 10650 850
F 0 "X2" H 10650 1050 50  0000 C CNN
F 1 "Conn_01x04_Female" H 10800 550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal" H 10650 850 50  0001 C CNN
F 3 "suddendocs.samtec.com/catalog_english/ssq_th.pdf" H 10650 850 50  0001 C CNN
F 4 "Digikey" H 10650 1050 50  0001 C CNN "Supplier"
F 5 "SAM1195-04-ND" H 10650 1050 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/samtec-inc/SSQ-104-02-T-S-RA/SAM1195-04-ND/1111408" H 10650 1050 50  0001 C CNN "Supplier Link"
F 7 "Samtec" H 10650 1050 50  0001 C CNN "Manufacturer"
F 8 "SSQ-104-02-T-S-RA" H 10650 1050 50  0001 C CNN "Manufacturer Part Number"
	1    10650 850 
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR03
U 1 1 5AB6309A
P 9650 1400
F 0 "#PWR03" H 9650 1250 50  0001 C CNN
F 1 "+12V" H 9650 1540 50  0000 C CNN
F 2 "" H 9650 1400 50  0001 C CNN
F 3 "" H 9650 1400 50  0001 C CNN
	1    9650 1400
	1    0    0    -1  
$EndComp
Text Label 9350 850  0    60   ~ 0
CH1_Green
Text Label 9350 950  0    60   ~ 0
CH1_Red
Text Label 9350 1050 0    60   ~ 0
CH1_Blue
$Comp
L Transistor_FET:IRF7403 Q3
U 1 1 5AB63B1C
P 8250 2900
F 0 "Q3" H 8600 3000 50  0000 L CNN
F 1 "IRF7401" H 8600 2900 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 2800 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 2900 50  0001 L CNN
F 4 "Digikey" H 8600 3000 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 3000 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 3000 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 3000 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 3000 50  0001 L CNN "Manufacturer Part Number"
	1    8250 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5AB63B2E
P 8000 3100
F 0 "R3" V 8080 3100 50  0000 C CNN
F 1 "10k" V 8000 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 3100 50  0001 C CNN
F 3 "" H 8000 3100 50  0001 C CNN
F 4 "Digikey" V 8080 3100 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 3100 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 3100 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 3100 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 3100 50  0001 C CNN "Manufacturer Part Number"
	1    8000 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5AB63B37
P 8000 3250
F 0 "#PWR05" H 8000 3000 50  0001 C CNN
F 1 "GND" H 8000 3100 50  0000 C CNN
F 2 "" H 8000 3250 50  0001 C CNN
F 3 "" H 8000 3250 50  0001 C CNN
	1    8000 3250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF7403 Q2
U 1 1 5AB63C3D
P 8250 2000
F 0 "Q2" H 8600 2100 50  0000 L CNN
F 1 "IRF7401" H 8600 2000 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 1900 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 2000 50  0001 L CNN
F 4 "Digikey" H 8600 2100 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 2100 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 2100 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 2100 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 2100 50  0001 L CNN "Manufacturer Part Number"
	1    8250 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5AB63C4F
P 8000 2200
F 0 "R2" V 8080 2200 50  0000 C CNN
F 1 "10k" V 8000 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 2200 50  0001 C CNN
F 3 "" H 8000 2200 50  0001 C CNN
F 4 "Digikey" V 8080 2200 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 2200 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 2200 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 2200 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 2200 50  0001 C CNN "Manufacturer Part Number"
	1    8000 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5AB63C58
P 8000 2350
F 0 "#PWR07" H 8000 2100 50  0001 C CNN
F 1 "GND" H 8000 2200 50  0000 C CNN
F 2 "" H 8000 2350 50  0001 C CNN
F 3 "" H 8000 2350 50  0001 C CNN
	1    8000 2350
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF7403 Q4
U 1 1 5AB6671F
P 8250 4100
F 0 "Q4" H 8600 4200 50  0000 L CNN
F 1 "IRF7401" H 8600 4100 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 4000 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 4100 50  0001 L CNN
F 4 "Digikey" H 8600 4200 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 4200 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 4200 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 4200 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 4200 50  0001 L CNN "Manufacturer Part Number"
	1    8250 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5AB66733
P 8000 4300
F 0 "R4" V 8080 4300 50  0000 C CNN
F 1 "10k" V 8000 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 4300 50  0001 C CNN
F 3 "" H 8000 4300 50  0001 C CNN
F 4 "Digikey" V 8080 4300 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 4300 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 4300 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 4300 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 4300 50  0001 C CNN "Manufacturer Part Number"
	1    8000 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5AB6673D
P 8000 4450
F 0 "#PWR09" H 8000 4200 50  0001 C CNN
F 1 "GND" H 8000 4300 50  0000 C CNN
F 2 "" H 8000 4450 50  0001 C CNN
F 3 "" H 8000 4450 50  0001 C CNN
	1    8000 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female X3
U 1 1 5AB66743
P 10650 3850
F 0 "X3" H 10650 4050 50  0000 C CNN
F 1 "Conn_01x04_Female" H 10800 3550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal" H 10650 3850 50  0001 C CNN
F 3 "suddendocs.samtec.com/catalog_english/ssq_th.pdf" H 10650 3850 50  0001 C CNN
F 4 "Digikey" H 10650 4050 50  0001 C CNN "Supplier"
F 5 "SAM1195-04-ND" H 10650 4050 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/samtec-inc/SSQ-104-02-T-S-RA/SAM1195-04-ND/1111408" H 10650 4050 50  0001 C CNN "Supplier Link"
F 7 "Samtec" H 10650 4050 50  0001 C CNN "Manufacturer"
F 8 "SSQ-104-02-T-S-RA" H 10650 4050 50  0001 C CNN "Manufacturer Part Number"
	1    10650 3850
	1    0    0    -1  
$EndComp
Text Label 9400 3850 0    60   ~ 0
CH2_Green
Text Label 9400 3950 0    60   ~ 0
CH2_Red
Text Label 9400 4050 0    60   ~ 0
CH2_Blue
$Comp
L Transistor_FET:IRF7403 Q6
U 1 1 5AB66760
P 8250 5900
F 0 "Q6" H 8600 6000 50  0000 L CNN
F 1 "IRF7401" H 8600 5900 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 5800 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 5900 50  0001 L CNN
F 4 "Digikey" H 8600 6000 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 6000 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 6000 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 6000 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 6000 50  0001 L CNN "Manufacturer Part Number"
	1    8250 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5AB66774
P 8000 6100
F 0 "R6" V 8080 6100 50  0000 C CNN
F 1 "10k" V 8000 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 6100 50  0001 C CNN
F 3 "" H 8000 6100 50  0001 C CNN
F 4 "Digikey" V 8080 6100 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 6100 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 6100 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 6100 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 6100 50  0001 C CNN "Manufacturer Part Number"
	1    8000 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5AB6677E
P 8000 6250
F 0 "#PWR011" H 8000 6000 50  0001 C CNN
F 1 "GND" H 8000 6100 50  0000 C CNN
F 2 "" H 8000 6250 50  0001 C CNN
F 3 "" H 8000 6250 50  0001 C CNN
	1    8000 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5AB667A3
P 8000 5200
F 0 "R5" V 8080 5200 50  0000 C CNN
F 1 "10k" V 8000 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 7930 5200 50  0001 C CNN
F 3 "" H 8000 5200 50  0001 C CNN
F 4 "Digikey" V 8080 5200 50  0001 C CNN "Supplier"
F 5 "311-10.0KHRCT-ND" V 8080 5200 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0710KL/311-10.0KHRCT-ND/729827" V 8080 5200 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 8080 5200 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0710KL" V 8080 5200 50  0001 C CNN "Manufacturer Part Number"
	1    8000 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5AB667AD
P 8000 5350
F 0 "#PWR013" H 8000 5100 50  0001 C CNN
F 1 "GND" H 8000 5200 50  0000 C CNN
F 2 "" H 8000 5350 50  0001 C CNN
F 3 "" H 8000 5350 50  0001 C CNN
	1    8000 5350
	1    0    0    -1  
$EndComp
$Comp
L ESP8266:NodeMCU1.0(ESP-12E) Mod1
U 1 1 5AB675E1
P 2950 2150
F 0 "Mod1" H 2950 3000 60  0000 C CNN
F 1 "NodeMCU1.0(ESP-12E)" H 2950 1300 60  0000 C CNN
F 2 "ESP8266:NodeMCU1.0(12-E)" H 2350 1300 60  0001 C CNN
F 3 "" H 2350 1300 60  0000 C CNN
	1    2950 2150
	1    0    0    -1  
$EndComp
Text Notes 750  800  0    118  ~ 0
NodeMCU
Text Notes 7200 750  0    118  ~ 0
Channel 1
Text Notes 7200 3750 0    118  ~ 0
Channel 2
$Comp
L regul:MC33063A IC1
U 1 1 5AB6B874
P 4000 5550
F 0 "IC1" H 3700 5825 79  0000 C CNN
F 1 "MC33063ADR" H 4000 5250 79  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3700 5550 79  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/mc33063a.pdf" H 3700 5550 79  0001 C CNN
F 4 "Texas Instruments" H 3700 5825 79  0001 C CNN "Manufacturer"
F 5 "Digikey" H 3700 5825 79  0001 C CNN "Supplier"
F 6 "296-17763-1-ND" H 3700 5825 79  0001 C CNN "Supplier Part Number"
F 7 "https://www.digikey.de/product-detail/de/texas-instruments/MC33063ADR/296-17763-1-ND/717455" H 3700 5825 79  0001 C CNN "Supplier Link"
F 8 "MC33063ADR" H 3700 5825 79  0001 C CNN "Manufacturer Part Number"
	1    4000 5550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5AB6B883
P 2150 2350
F 0 "#PWR014" H 2150 2100 50  0001 C CNN
F 1 "GND" H 2150 2200 50  0000 C CNN
F 2 "" H 2150 2350 50  0001 C CNN
F 3 "" H 2150 2350 50  0001 C CNN
	1    2150 2350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5AB6B8C5
P 3750 2750
F 0 "#PWR015" H 3750 2500 50  0001 C CNN
F 1 "GND" H 3750 2600 50  0000 C CNN
F 2 "" H 3750 2750 50  0001 C CNN
F 3 "" H 3750 2750 50  0001 C CNN
	1    3750 2750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5AB6B900
P 3750 2050
F 0 "#PWR016" H 3750 1800 50  0001 C CNN
F 1 "GND" H 3750 1900 50  0000 C CNN
F 2 "" H 3750 2050 50  0001 C CNN
F 3 "" H 3750 2050 50  0001 C CNN
	1    3750 2050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5AB6B93B
P 2150 2750
F 0 "#PWR017" H 2150 2500 50  0001 C CNN
F 1 "GND" H 2150 2600 50  0000 C CNN
F 2 "" H 2150 2750 50  0001 C CNN
F 3 "" H 2150 2750 50  0001 C CNN
	1    2150 2750
	0    1    1    0   
$EndComp
$Comp
L Connector:Jack-DC X1
U 1 1 5AB6BBF9
P 750 5600
F 0 "X1" H 750 5810 50  0000 C CNN
F 1 "Jack-DC" H 750 5425 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 800 5560 50  0001 C CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/pj-102a.pdf" H 800 5560 50  0001 C CNN
F 4 "Digikey" H 750 5810 50  0001 C CNN "Supplier"
F 5 "CP-102A-ND" H 750 5810 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/cui-inc/PJ-102A/CP-102A-ND/275425" H 750 5810 50  0001 C CNN "Supplier Link"
F 7 "CUI" H 750 5810 50  0001 C CNN "Manufacturer"
F 8 "PJ-102A" H 750 5810 50  0001 C CNN "Manufacturer Part Number"
	1    750  5600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR018
U 1 1 5AB6BFAC
P 2150 5350
F 0 "#PWR018" H 2150 5200 50  0001 C CNN
F 1 "+12V" H 2150 5490 50  0000 C CNN
F 2 "" H 2150 5350 50  0001 C CNN
F 3 "" H 2150 5350 50  0001 C CNN
	1    2150 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5AB6C6AE
P 1100 6000
F 0 "#PWR019" H 1100 5750 50  0001 C CNN
F 1 "GND" H 1100 5850 50  0000 C CNN
F 2 "" H 1100 6000 50  0001 C CNN
F 3 "" H 1100 6000 50  0001 C CNN
	1    1100 6000
	1    0    0    -1  
$EndComp
Text Notes 650  4650 0    118  ~ 0
Power
$Comp
L Device:R R9
U 1 0 5C83EEE7
P 3150 5600
F 0 "R9" V 3230 5600 50  0000 C CNN
F 1 "1" V 3150 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 3080 5600 50  0001 C CNN
F 3 "" H 3150 5600 50  0001 C CNN
F 4 "Digikey" V 3230 5600 50  0001 C CNN "Supplier"
F 5 "311-1.00HRCT-ND" V 3230 5600 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-071RL/311-1.00HRCT-ND/729789" V 3230 5600 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 3230 5600 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-071RL" V 3230 5600 50  0001 C CNN "Manufacturer Part Number"
	1    3150 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5AB78DF6
P 2600 5850
F 0 "C2" H 2625 5950 50  0000 L CNN
F 1 "22u/25V" H 2625 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 2638 5700 50  0001 C CNN
F 3 "" H 2600 5850 50  0001 C CNN
F 4 "Digikey" H 2625 5950 50  0001 L CNN "Supplier"
F 5 "1276-3047-1-ND" H 2625 5950 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL31A226KAHNNNE/1276-3047-1-ND/3891133" H 2625 5950 50  0001 L CNN "Supplier Link"
F 7 "Samsung" H 2625 5950 50  0001 L CNN "Manufacturer"
F 8 "CL31A226KAHNNNE" H 2625 5950 50  0001 L CNN "Manufacturer Part Number"
	1    2600 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D1
U 1 1 5AB78F56
P 5350 5900
F 0 "D1" H 5350 6000 50  0000 C CNN
F 1 "B160-13-F" H 5350 5800 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 5350 5900 50  0001 C CNN
F 3 "http://www.diodes.com/_files/datasheets/ds13002.pdf" H 5350 5900 50  0001 C CNN
F 4 "Digikey" H 5350 6000 50  0001 C CNN "Supplier"
F 5 "B160-FDICT-ND" H 5350 6000 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/diodes-incorporated/B160-13-F/B160-FDICT-ND/806561" H 5350 6000 50  0001 C CNN "Supplier Link"
F 7 "Diodes Inc" H 5350 6000 50  0001 C CNN "Manufacturer"
F 8 "D160-13-F" H 5350 6000 50  0001 C CNN "Manufacturer Part Number"
	1    5350 5900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5AB79EAE
P 5350 6100
F 0 "#PWR020" H 5350 5850 50  0001 C CNN
F 1 "GND" H 5350 5950 50  0000 C CNN
F 2 "" H 5350 6100 50  0001 C CNN
F 3 "" H 5350 6100 50  0001 C CNN
	1    5350 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Core_Iron L1
U 1 1 5AB7A2B2
P 5600 5600
F 0 "L1" V 5550 5600 50  0000 C CNN
F 1 "47u" V 5710 5600 50  0000 C CNN
F 2 "Inductor_SMD:L_2816_7142Metric_Pad2.42x4.50mm_HandSolder" H 5600 5600 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/P02/JELF243A-0093.pdf" H 5600 5600 50  0001 C CNN
F 4 "Murata" V 5550 5600 50  0001 C CNN "Manufacturer"
F 5 "" V 5550 5600 50  0001 C CNN "Supplier Part Number"
F 6 "Digikey" V 5550 5600 50  0001 C CNN "Supplier"
F 7 "2.8mm" V 5550 5600 50  0001 C CNN "Height"
F 8 "https://www.digikey.de/product-detail/de/murata-electronics-north-america/LQH43PN470M26L/490-12050-1-ND/5403097" V 5550 5600 50  0001 C CNN "Supplier Link"
F 9 "LQH43PN470M26L" V 5550 5600 50  0001 C CNN "Manufacturer Part Number"
	1    5600 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5AB7A668
P 5850 6100
F 0 "#PWR021" H 5850 5850 50  0001 C CNN
F 1 "GND" H 5850 5950 50  0000 C CNN
F 2 "" H 5850 6100 50  0001 C CNN
F 3 "" H 5850 6100 50  0001 C CNN
	1    5850 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5AB7AA2B
P 4650 5350
F 0 "#PWR022" H 4650 5100 50  0001 C CNN
F 1 "GND" H 4650 5200 50  0000 C CNN
F 2 "" H 4650 5350 50  0001 C CNN
F 3 "" H 4650 5350 50  0001 C CNN
	1    4650 5350
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C1
U 1 1 5AB7AD1A
P 2350 5850
F 0 "C1" H 2375 5950 50  0000 L CNN
F 1 "100u/16V" H 1950 5750 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 2388 5700 50  0001 C CNN
F 3 "" H 2350 5850 50  0001 C CNN
F 4 "493-2105-1-ND" H 2375 5950 50  0001 L CNN "Supplier Part Number"
F 5 "Digikey" H 2375 5950 50  0001 L CNN "Supplier"
F 6 "7.7mm" H 2375 5950 50  0001 L CNN "Heigth"
F 7 "D" H 2375 5950 50  0001 L CNN "Sizecode"
F 8 "https://www.digikey.de/product-detail/de/nichicon/UWX1C101MCL1GB/493-2105-1-ND/590080" H 2375 5950 50  0001 L CNN "Supplier Link"
F 9 "Nichicon" H 2375 5950 50  0001 L CNN "Manufacturer"
F 10 "UWX1C101MCL1GB" H 2375 5950 50  0001 L CNN "Manufacturer Part Number"
	1    2350 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5AB7B273
P 4800 5900
F 0 "C5" H 4825 6000 50  0000 L CNN
F 1 "47p" H 4825 5800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 5750 50  0001 C CNN
F 3 "" H 4800 5900 50  0001 C CNN
F 4 "Digikey" H 4825 6000 50  0001 L CNN "Supplier"
F 5 "399-15802-6-ND" H 4825 6000 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/kemet/C0603C470K5RACAUTO/399-15802-6-ND/7516568" H 4825 6000 50  0001 L CNN "Supplier Link"
F 7 "Kemet" H 4825 6000 50  0001 L CNN "Manufacturer"
F 8 "C0603C470K5RACAUTO" H 4825 6000 50  0001 L CNN "Manufacturer Part Number"
	1    4800 5900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5AB7BC5F
P 4800 6100
F 0 "#PWR023" H 4800 5850 50  0001 C CNN
F 1 "GND" H 4800 5950 50  0000 C CNN
F 2 "" H 4800 6100 50  0001 C CNN
F 3 "" H 4800 6100 50  0001 C CNN
	1    4800 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5AB7CE2C
P 2600 6000
F 0 "#PWR024" H 2600 5750 50  0001 C CNN
F 1 "GND" H 2600 5850 50  0000 C CNN
F 2 "" H 2600 6000 50  0001 C CNN
F 3 "" H 2600 6000 50  0001 C CNN
	1    2600 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5AB7CE82
P 2350 6000
F 0 "#PWR025" H 2350 5750 50  0001 C CNN
F 1 "GND" H 2350 5850 50  0000 C CNN
F 2 "" H 2350 6000 50  0001 C CNN
F 3 "" H 2350 6000 50  0001 C CNN
	1    2350 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 0 5C83EEE8
P 3150 5000
F 0 "R7" V 3230 5000 50  0000 C CNN
F 1 "11k" V 3150 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 3080 5000 50  0001 C CNN
F 3 "" H 3150 5000 50  0001 C CNN
F 4 "Digikey" V 3230 5000 50  0001 C CNN "Supplier"
F 5 "311-11.5KHRCT-ND" V 3230 5000 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0711K5L/311-11.5KHRCT-ND/729848" V 3230 5000 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 3230 5000 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0711K5L" V 3230 5000 50  0001 C CNN "Manufacturer Part Number"
	1    3150 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 0 5C83EEE9
P 3550 5000
F 0 "R8" V 3630 5000 50  0000 C CNN
F 1 "18k" V 3550 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 3480 5000 50  0001 C CNN
F 3 "" H 3550 5000 50  0001 C CNN
F 4 "Digikey" V 3630 5000 50  0001 C CNN "Supplier"
F 5 "311-18.0KHRCT-ND" V 3630 5000 50  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-0718KL/311-18.0KHRCT-ND/729936" V 3630 5000 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 3630 5000 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-0718KL" V 3630 5000 50  0001 C CNN "Manufacturer Part Number"
	1    3550 5000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5AB7DDAB
P 2950 5000
F 0 "#PWR026" H 2950 4750 50  0001 C CNN
F 1 "GND" H 2950 4850 50  0000 C CNN
F 2 "" H 2950 5000 50  0001 C CNN
F 3 "" H 2950 5000 50  0001 C CNN
	1    2950 5000
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR027
U 1 1 5AB7FDC9
P 1300 2450
F 0 "#PWR027" H 1300 2300 50  0001 C CNN
F 1 "+3.3V" H 1300 2590 50  0000 C CNN
F 2 "" H 1300 2450 50  0001 C CNN
F 3 "" H 1300 2450 50  0001 C CNN
	1    1300 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR028
U 1 1 5AB7FE13
P 6450 5600
F 0 "#PWR028" H 6450 5450 50  0001 C CNN
F 1 "+3.3V" V 6350 5650 50  0000 C CNN
F 2 "" H 6450 5600 50  0001 C CNN
F 3 "" H 6450 5600 50  0001 C CNN
	1    6450 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5AB7F70C
P 5850 5850
F 0 "C3" H 5875 5950 50  0000 L CNN
F 1 "22u/25V" H 5875 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 5888 5700 50  0001 C CNN
F 3 "" H 5850 5850 50  0001 C CNN
F 4 "Digikey" H 5850 5850 60  0001 C CNN "Supplier"
F 5 "1276-3047-1-ND" H 5850 5850 60  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL31A226KAHNNNE/1276-3047-1-ND/3891133" H 5875 5950 50  0001 L CNN "Supplier Link"
F 7 "Samsung" H 5875 5950 50  0001 L CNN "Manufacturer"
F 8 "CL31A226KAHNNNE" H 5875 5950 50  0001 L CNN "Manufacturer Part Number"
	1    5850 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5AB7FB86
P 6250 5850
F 0 "C4" H 6275 5950 50  0000 L CNN
F 1 "22u/25V" H 6275 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 6288 5700 50  0001 C CNN
F 3 "" H 6250 5850 50  0001 C CNN
F 4 "Digikey" H 6250 5850 60  0001 C CNN "Supplier"
F 5 "1276-3047-1-ND" H 6250 5850 60  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL31A226KAHNNNE/1276-3047-1-ND/3891133" H 6275 5950 50  0001 L CNN "Supplier Link"
F 7 "Samsung" H 6275 5950 50  0001 L CNN "Manufacturer"
F 8 "CL31A226KAHNNNE" H 6275 5950 50  0001 L CNN "Manufacturer Part Number"
	1    6250 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5AB806B1
P 6250 6100
F 0 "#PWR029" H 6250 5850 50  0001 C CNN
F 1 "GND" H 6250 5950 50  0000 C CNN
F 2 "" H 6250 6100 50  0001 C CNN
F 3 "" H 6250 6100 50  0001 C CNN
	1    6250 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 0 00000000
P 3150 5800
F 0 "R10" V 3230 5800 50  0000 C CNN
F 1 "1" V 3150 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.84x1.00mm_HandSolder" V 3080 5800 50  0001 C CNN
F 3 "" H 3150 5800 50  0001 C CNN
F 4 "Digikey" V 3150 5800 60  0001 C CNN "Supplier"
F 5 "311-1.00HRCT-ND" V 3150 5800 60  0001 C CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/yageo/RC0603FR-071RL/311-1.00HRCT-ND/729789" V 3230 5800 50  0001 C CNN "Supplier Link"
F 7 "Yageo" V 3230 5800 50  0001 C CNN "Manufacturer"
F 8 "RC0603FR-071RL" V 3230 5800 50  0001 C CNN "Manufacturer Part Number"
	1    3150 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	8350 1300 8350 1350
Wire Wire Line
	7450 1100 8000 1100
Wire Wire Line
	8000 1150 8000 1100
Connection ~ 8000 1100
Wire Wire Line
	9300 1050 10450 1050
Wire Wire Line
	10100 750  10450 750 
Wire Wire Line
	8350 900  8350 850 
Wire Wire Line
	8350 3100 8350 3150
Wire Wire Line
	7450 2900 8000 2900
Wire Wire Line
	8000 2950 8000 2900
Connection ~ 8000 2900
Wire Wire Line
	8350 2700 8350 2650
Wire Wire Line
	8350 2200 8350 2250
Wire Wire Line
	7450 2000 8000 2000
Wire Wire Line
	8000 2050 8000 2000
Connection ~ 8000 2000
Wire Wire Line
	8350 1800 8350 1750
Wire Wire Line
	9300 2650 9300 1050
Wire Wire Line
	9100 950  10450 950 
Wire Wire Line
	9100 1750 9100 950 
Wire Wire Line
	8350 4300 8350 4350
Wire Wire Line
	9300 4050 10450 4050
Wire Wire Line
	10100 3750 10450 3750
Wire Wire Line
	8350 3900 8350 3850
Wire Wire Line
	8350 6100 8350 6150
Wire Wire Line
	7450 5900 8000 5900
Wire Wire Line
	8000 5950 8000 5900
Connection ~ 8000 5900
Wire Wire Line
	8350 5700 8350 5650
Wire Wire Line
	8350 5200 8350 5250
Wire Wire Line
	7450 5000 8000 5000
Wire Wire Line
	8000 5050 8000 5000
Connection ~ 8000 5000
Wire Wire Line
	8350 4800 8350 4750
Wire Wire Line
	9300 5650 9300 4050
Wire Wire Line
	9100 3950 10450 3950
Wire Wire Line
	9100 4750 9100 3950
Wire Wire Line
	1050 5700 1100 5700
Wire Wire Line
	1100 5600 1100 5700
Wire Wire Line
	5350 5750 5350 5600
Wire Wire Line
	4600 5600 5350 5600
Wire Wire Line
	5350 6100 5350 6050
Connection ~ 5350 5600
Wire Wire Line
	5850 6100 5850 6000
Wire Wire Line
	5850 5700 5850 5600
Wire Wire Line
	5750 5600 5850 5600
Wire Wire Line
	4650 5400 4600 5400
Connection ~ 5850 5600
Wire Wire Line
	4650 5400 4650 5350
Wire Wire Line
	4800 5750 4800 5500
Wire Wire Line
	4800 5500 4600 5500
Wire Wire Line
	4800 6100 4800 6050
Wire Wire Line
	2600 5500 2600 5700
Wire Wire Line
	2350 5500 2350 5700
Connection ~ 2600 5500
Wire Wire Line
	4600 5700 4650 5700
Wire Wire Line
	4650 5700 4650 5900
Wire Wire Line
	4650 5900 3350 5900
Wire Wire Line
	3350 5900 3350 5800
Wire Wire Line
	3300 5600 3350 5600
Wire Wire Line
	3400 5700 3350 5700
Connection ~ 3350 5700
Connection ~ 3350 5600
Wire Wire Line
	3000 5600 2950 5600
Wire Wire Line
	2950 5500 2950 5600
Connection ~ 2950 5500
Wire Wire Line
	3000 5000 2950 5000
Wire Wire Line
	3400 5400 3350 5400
Wire Wire Line
	3350 5400 3350 5000
Wire Wire Line
	3300 5000 3350 5000
Connection ~ 3350 5000
Wire Wire Line
	3700 5000 6000 5000
Wire Wire Line
	6000 5000 6000 5600
Connection ~ 6000 5600
Connection ~ 2350 5500
Wire Wire Line
	6250 6100 6250 6000
Wire Wire Line
	6250 5700 6250 5600
Connection ~ 6250 5600
Wire Wire Line
	3300 5800 3350 5800
Connection ~ 3350 5800
Wire Wire Line
	2950 5800 3000 5800
Connection ~ 2950 5600
Connection ~ 8000 4100
Wire Wire Line
	7450 4100 8000 4100
Wire Wire Line
	8000 4150 8000 4100
Text Label 7450 1100 0    60   ~ 0
PWM1_Green
Text Label 7450 2000 0    60   ~ 0
PWM1_Red
Text Label 7450 2900 0    60   ~ 0
PWM1_Blue
Text Label 7450 4100 0    60   ~ 0
PWM2_Green
Text Label 7450 5000 0    60   ~ 0
PWM2_Red
Text Label 7450 5900 0    60   ~ 0
PWM2_Blue
Wire Wire Line
	4850 1650 3750 1650
Wire Wire Line
	4850 1750 3750 1750
Wire Wire Line
	4850 1550 3750 1550
Wire Wire Line
	4850 2350 3750 2350
Wire Wire Line
	4850 2150 3750 2150
Wire Wire Line
	4850 2250 3750 2250
Text Label 4850 1650 2    60   ~ 0
PWM1_Blue
Text Label 4850 1550 2    60   ~ 0
PWM1_Red
Text Label 4850 1750 2    60   ~ 0
PWM1_Green
Text Label 4850 2250 2    60   ~ 0
PWM2_Red
Text Label 4850 2150 2    60   ~ 0
PWM2_Green
Text Label 4850 2350 2    60   ~ 0
PWM2_Blue
NoConn ~ 3750 1950
NoConn ~ 3750 2850
Wire Wire Line
	1300 2450 1600 2450
Wire Wire Line
	1600 2500 1600 2450
Connection ~ 1600 2450
Text Label 4850 5600 0    60   ~ 0
Switchnode
$Comp
L Connector_Generic:Conn_01x01 H1
U 1 1 5AEF5614
P 1050 6500
F 0 "H1" H 1050 6600 50  0000 C CNN
F 1 "Conn_01x01" H 1050 6400 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6500 50  0000 C CNN
F 3 "" H 1050 6500 50  0001 C CNN
	1    1050 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 H2
U 1 1 5AEF5CAF
P 1050 6800
F 0 "H2" H 1050 6900 50  0000 C CNN
F 1 "Conn_01x01" H 1050 6700 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 6800 50  0000 C CNN
F 3 "" H 1050 6800 50  0001 C CNN
	1    1050 6800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 H3
U 1 1 5AEF5D22
P 1050 7100
F 0 "H3" H 1050 7200 50  0000 C CNN
F 1 "Conn_01x01" H 1050 7000 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 7100 50  0000 C CNN
F 3 "" H 1050 7100 50  0001 C CNN
	1    1050 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 H4
U 1 1 5AEF5D96
P 1050 7400
F 0 "H4" H 1050 7500 50  0000 C CNN
F 1 "Conn_01x01" H 1050 7300 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1950 7400 50  0000 C CNN
F 3 "" H 1050 7400 50  0001 C CNN
	1    1050 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5AEF6A4D
P 750 7450
F 0 "#PWR030" H 750 7200 50  0001 C CNN
F 1 "GND" H 750 7300 50  0000 C CNN
F 2 "" H 750 7450 50  0001 C CNN
F 3 "" H 750 7450 50  0001 C CNN
	1    750  7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  6500 750  6800
Wire Wire Line
	750  6500 850  6500
Wire Wire Line
	850  6800 750  6800
Connection ~ 750  6800
Wire Wire Line
	850  7100 750  7100
Connection ~ 750  7100
Wire Wire Line
	850  7400 750  7400
Connection ~ 750  7400
$Comp
L Device:C C6
U 1 1 5AEF870F
P 1600 2650
F 0 "C6" H 1625 2750 50  0000 L CNN
F 1 "100n" H 1625 2550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1638 2500 50  0001 C CNN
F 3 "" H 1600 2650 50  0001 C CNN
F 4 "Digikey" H 1625 2750 50  0001 L CNN "Supplier"
F 5 "445-5667-1-ND" H 1625 2750 50  0001 L CNN "Supplier Part Number"
F 6 "TDK" H 1625 2750 50  0001 L CNN "Manufacturer"
F 7 "https://www.digikey.de/products/de?keywords=445-5667-1-ND" H 1625 2750 50  0001 L CNN "Supplier Link"
F 8 "CGA3E2X7R1E104K080AA" H 1625 2750 50  0001 L CNN "Manufacturer Part Number"
	1    1600 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR031
U 1 1 5AEF9331
P 1600 2850
F 0 "#PWR031" H 1600 2600 50  0001 C CNN
F 1 "GND" H 1600 2700 50  0000 C CNN
F 2 "" H 1600 2850 50  0001 C CNN
F 3 "" H 1600 2850 50  0001 C CNN
	1    1600 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2850 1600 2800
$Comp
L Device:C C7
U 1 1 5AEF6663
P 10100 1700
F 0 "C7" H 10125 1800 50  0000 L CNN
F 1 "100n" H 10125 1600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10138 1550 50  0001 C CNN
F 3 "" H 10100 1700 50  0001 C CNN
F 4 "Digikey" H 10125 1800 50  0001 L CNN "Supplier"
F 5 "445-5667-1-ND" H 10125 1800 50  0001 L CNN "Supplier Part Number"
F 6 "TDK" H 10125 1800 50  0001 L CNN "Manufacturer"
F 7 "https://www.digikey.de/products/de?keywords=445-5667-1-ND" H 10125 1800 50  0001 L CNN "Supplier Link"
F 8 "CGA3E2X7R1E104K080AA" H 10125 1800 50  0001 L CNN "Manufacturer Part Number"
	1    10100 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5AEF7CB0
P 10100 1900
F 0 "#PWR032" H 10100 1650 50  0001 C CNN
F 1 "GND" H 10100 1750 50  0000 C CNN
F 2 "" H 10100 1900 50  0001 C CNN
F 3 "" H 10100 1900 50  0001 C CNN
	1    10100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1900 10100 1850
Wire Wire Line
	9700 1450 9650 1450
Wire Wire Line
	9650 1450 9650 1400
Wire Wire Line
	9900 1450 10100 1450
Wire Wire Line
	10100 750  10100 1450
Connection ~ 10100 1450
$Comp
L power:+12V #PWR033
U 1 1 5AEFFB7B
P 9650 4400
F 0 "#PWR033" H 9650 4250 50  0001 C CNN
F 1 "+12V" H 9650 4540 50  0000 C CNN
F 2 "" H 9650 4400 50  0001 C CNN
F 3 "" H 9650 4400 50  0001 C CNN
	1    9650 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5AEFFB87
P 10100 4700
F 0 "C8" H 10125 4800 50  0000 L CNN
F 1 "100n" H 10125 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10138 4550 50  0001 C CNN
F 3 "" H 10100 4700 50  0001 C CNN
F 4 "Digikey" H 10125 4800 50  0001 L CNN "Supplier"
F 5 "445-5667-1-ND" H 10125 4800 50  0001 L CNN "Supplier Part Number"
F 6 "TDK" H 10125 4800 50  0001 L CNN "Manufacturer"
F 7 "https://www.digikey.de/products/de?keywords=445-5667-1-ND" H 10125 4800 50  0001 L CNN "Supplier Link"
F 8 "CGA3E2X7R1E104K080AA" H 10125 4800 50  0001 L CNN "Manufacturer Part Number"
	1    10100 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR034
U 1 1 5AEFFB8D
P 10100 4900
F 0 "#PWR034" H 10100 4650 50  0001 C CNN
F 1 "GND" H 10100 4750 50  0000 C CNN
F 2 "" H 10100 4900 50  0001 C CNN
F 3 "" H 10100 4900 50  0001 C CNN
	1    10100 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 4900 10100 4850
Wire Wire Line
	9700 4450 9650 4450
Wire Wire Line
	9650 4450 9650 4400
Wire Wire Line
	9900 4450 10100 4450
Wire Wire Line
	10100 3750 10100 4450
Connection ~ 10100 4450
$Comp
L Device:Ferrite_Bead_Small L2
U 1 1 5AF00EE0
P 9800 1450
F 0 "L2" V 9900 1400 50  0000 L CNN
F 1 "Ferrite Z120 6A" V 9700 1250 50  0000 L CNN
F 2 "Inductor_SMD:L_1206_3216Metric" V 9730 1450 50  0001 C CNN
F 3 "" H 9800 1450 50  0001 C CNN
F 4 "Digikey" V 9800 1450 60  0001 C CNN "Supplier"
F 5 "490-16525-1-ND " V 9800 1450 60  0001 C CNN "Supplier Part Number"
F 6 "Murata" V 9800 1450 60  0001 C CNN "Manufacturer"
F 7 "BLM31KN121SZ1L" V 9800 1450 60  0001 C CNN "Manufacturer Part Number"
	1    9800 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead_Small L3
U 1 1 5AF01B43
P 9800 4450
F 0 "L3" V 9900 4400 50  0000 L CNN
F 1 "Ferrite Z120 6A" V 9700 4250 50  0000 L CNN
F 2 "Inductor_SMD:L_1206_3216Metric" V 9730 4450 50  0001 C CNN
F 3 "" H 9800 4450 50  0001 C CNN
F 4 "Digikey" V 9800 4450 60  0001 C CNN "Supplier"
F 5 "490-16525-1-ND " V 9800 4450 60  0001 C CNN "Supplier Part Number"
F 6 "Murata" V 9800 4450 60  0001 C CNN "Manufacturer"
F 7 "BLM31KN121SZ1L" V 9800 4450 60  0001 C CNN "Manufacturer Part Number"
	1    9800 4450
	0    -1   -1   0   
$EndComp
$Comp
L Device:Ferrite_Bead_Small L4
U 1 1 5AF87BCF
P 1600 5500
F 0 "L4" V 1700 5450 50  0000 L CNN
F 1 "Ferrite Z120 6A" V 1500 5300 50  0000 L CNN
F 2 "Inductor_SMD:L_1206_3216Metric" V 1530 5500 50  0001 C CNN
F 3 "" H 1600 5500 50  0001 C CNN
F 4 "Digikey" V 1600 5500 60  0001 C CNN "Supplier"
F 5 "490-16525-1-ND " V 1600 5500 60  0001 C CNN "Supplier Part Number"
F 6 "Murata" V 1600 5500 60  0001 C CNN "Manufacturer"
F 7 "BLM31KN121SZ1L" V 1600 5500 60  0001 C CNN "Manufacturer Part Number"
	1    1600 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 5500 1800 5500
$Comp
L Device:C C9
U 1 1 5AF8C9CD
P 1400 5750
F 0 "C9" H 1425 5850 50  0000 L CNN
F 1 "100n" H 1425 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1438 5600 50  0001 C CNN
F 3 "" H 1400 5750 50  0001 C CNN
F 4 "Digikey" H 1425 5850 50  0001 L CNN "Supplier"
F 5 "445-5667-1-ND" H 1425 5850 50  0001 L CNN "Supplier Part Number"
F 6 "TDK" H 1425 5850 50  0001 L CNN "Manufacturer"
F 7 "https://www.digikey.de/products/de?keywords=445-5667-1-ND" H 1425 5850 50  0001 L CNN "Supplier Link"
F 8 "CGA3E2X7R1E104K080AA" H 1425 5850 50  0001 L CNN "Manufacturer Part Number"
	1    1400 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5AF8CF49
P 1800 5750
F 0 "C10" H 1825 5850 50  0000 L CNN
F 1 "100n" H 1825 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1838 5600 50  0001 C CNN
F 3 "" H 1800 5750 50  0001 C CNN
F 4 "Digikey" H 1825 5850 50  0001 L CNN "Supplier"
F 5 "445-5667-1-ND" H 1825 5850 50  0001 L CNN "Supplier Part Number"
F 6 "TDK" H 1825 5850 50  0001 L CNN "Manufacturer"
F 7 "https://www.digikey.de/products/de?keywords=445-5667-1-ND" H 1825 5850 50  0001 L CNN "Supplier Link"
F 8 "CGA3E2X7R1E104K080AA" H 1825 5850 50  0001 L CNN "Manufacturer Part Number"
	1    1800 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 5600 1800 5500
Connection ~ 1800 5500
$Comp
L power:GND #PWR035
U 1 1 5AF8E68B
P 1400 6000
F 0 "#PWR035" H 1400 5750 50  0001 C CNN
F 1 "GND" H 1400 5850 50  0000 C CNN
F 2 "" H 1400 6000 50  0001 C CNN
F 3 "" H 1400 6000 50  0001 C CNN
	1    1400 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5AF8E70E
P 1800 6000
F 0 "#PWR036" H 1800 5750 50  0001 C CNN
F 1 "GND" H 1800 5850 50  0000 C CNN
F 2 "" H 1800 6000 50  0001 C CNN
F 3 "" H 1800 6000 50  0001 C CNN
	1    1800 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 5900 1800 6000
Wire Wire Line
	1400 5900 1400 6000
Wire Wire Line
	1400 5600 1400 5500
Wire Wire Line
	1050 5500 1400 5500
Connection ~ 1400 5500
Wire Wire Line
	2150 5350 2150 5500
Connection ~ 2150 5500
Text Label 1050 5500 0    60   ~ 0
12V_in
Wire Wire Line
	1050 5600 1100 5600
Connection ~ 1100 5700
Wire Wire Line
	8000 1100 8050 1100
Wire Wire Line
	8000 2900 8050 2900
Wire Wire Line
	8000 2000 8050 2000
Wire Wire Line
	8000 5900 8050 5900
Wire Wire Line
	8000 5000 8050 5000
Wire Wire Line
	5350 5600 5450 5600
Wire Wire Line
	5850 5600 6000 5600
Wire Wire Line
	2600 5500 2950 5500
Wire Wire Line
	3350 5700 3350 5600
Wire Wire Line
	3350 5600 3400 5600
Wire Wire Line
	2950 5500 3400 5500
Wire Wire Line
	3350 5000 3400 5000
Wire Wire Line
	6000 5600 6250 5600
Wire Wire Line
	2350 5500 2600 5500
Wire Wire Line
	6250 5600 6450 5600
Wire Wire Line
	3350 5800 3350 5700
Wire Wire Line
	2950 5600 2950 5800
Wire Wire Line
	8000 4100 8050 4100
Wire Wire Line
	1600 2450 2150 2450
Wire Wire Line
	750  6800 750  7100
Wire Wire Line
	750  7100 750  7400
Wire Wire Line
	750  7400 750  7450
Wire Wire Line
	10100 1450 10100 1550
Wire Wire Line
	10100 4450 10100 4550
Wire Wire Line
	1800 5500 2150 5500
Wire Wire Line
	1400 5500 1500 5500
Wire Wire Line
	2150 5500 2350 5500
Wire Wire Line
	1100 5700 1100 6000
Wire Wire Line
	8350 5650 9300 5650
$Comp
L power:GND #PWR0101
U 1 1 5C845C19
P 8350 1350
F 0 "#PWR0101" H 8350 1100 50  0001 C CNN
F 1 "GND" H 8350 1200 50  0000 C CNN
F 2 "" H 8350 1350 50  0001 C CNN
F 3 "" H 8350 1350 50  0001 C CNN
	1    8350 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5C845CEC
P 8350 2250
F 0 "#PWR0102" H 8350 2000 50  0001 C CNN
F 1 "GND" H 8350 2100 50  0000 C CNN
F 2 "" H 8350 2250 50  0001 C CNN
F 3 "" H 8350 2250 50  0001 C CNN
	1    8350 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C845EFF
P 8350 3150
F 0 "#PWR0103" H 8350 2900 50  0001 C CNN
F 1 "GND" H 8350 3000 50  0000 C CNN
F 2 "" H 8350 3150 50  0001 C CNN
F 3 "" H 8350 3150 50  0001 C CNN
	1    8350 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C8460C2
P 8350 4350
F 0 "#PWR0104" H 8350 4100 50  0001 C CNN
F 1 "GND" H 8350 4200 50  0000 C CNN
F 2 "" H 8350 4350 50  0001 C CNN
F 3 "" H 8350 4350 50  0001 C CNN
	1    8350 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C846290
P 8350 6150
F 0 "#PWR0105" H 8350 5900 50  0001 C CNN
F 1 "GND" H 8350 6000 50  0000 C CNN
F 2 "" H 8350 6150 50  0001 C CNN
F 3 "" H 8350 6150 50  0001 C CNN
	1    8350 6150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5C84611D
P 8350 5250
F 0 "#PWR0106" H 8350 5000 50  0001 C CNN
F 1 "GND" H 8350 5100 50  0000 C CNN
F 2 "" H 8350 5250 50  0001 C CNN
F 3 "" H 8350 5250 50  0001 C CNN
	1    8350 5250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF7403 Q5
U 1 1 5AB6678F
P 8250 5000
F 0 "Q5" H 8600 5100 50  0000 L CNN
F 1 "IRF7401" H 8600 5000 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 8600 4900 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf7401.pdf?fileId=5546d462533600a4015355fa03d91b94" H 8150 5000 50  0001 L CNN
F 4 "Digikey" H 8600 5100 50  0001 L CNN "Supplier"
F 5 "IRF7401PBFCT-ND" H 8600 5100 50  0001 L CNN "Supplier Part Number"
F 6 "https://www.digikey.de/product-detail/de/infineon-technologies/IRF7401TRPBF/IRF7401PBFCT-ND/812582" H 8600 5100 50  0001 L CNN "Supplier Link"
F 7 "Infineon" H 8600 5100 50  0001 L CNN "Manufacturer"
F 8 "IRF7401TRPBF" H 8600 5100 50  0001 L CNN "Manufacturer Part Number"
	1    8250 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 850  10450 850 
Wire Wire Line
	8350 1750 9100 1750
Wire Wire Line
	8350 2650 9300 2650
Wire Wire Line
	8350 3850 10450 3850
Wire Wire Line
	8350 4750 9100 4750
Text Notes 7350 7500 0    79   ~ 16
colorNode
Text Notes 10550 7650 0    79   ~ 16
v1.00
$EndSCHEMATC
