colorNode is an MQTT controlled RGB dimmer based on nodeMCU

# Cloning
In order to get all kicad library elements, clone this repository recursively:

```
git clone --recursive <URL>
```


# Hardware
* Based on NodeMCU / ESP8266
* DIY-Friendly design
* WiFi
* 12V input voltage
* 12V PWM outputs
* 10A maximum output current per channel

# Software
Uses ESPHome, for instructions see [https://esphome.io/]. A configuration file can be found in the esphome directory.
This file contains the SSID and password.

Compile and install as follows:
```
cd esphome
docker run --rm -v "${PWD}":/config --device=/dev/ttyUSB0 -it esphome/esphome colorNode.yaml run
```
